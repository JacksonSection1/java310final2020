//Jackson Kwan
package finalExamJava;

import javafx.application.*;
import javafx.stage.*;
import javafx.scene.*;
import javafx.scene.control.*;
import javafx.scene.layout.*;
import javafx.scene.text.*;


//DiceRoll class that shows the GUI to the user and the betting menu.
public class DiceRoll extends Application{
	public void start(Stage stage) {
		int startMoney = 500;
		Group root = new Group();
		
		//scene
		Scene sc = new Scene(root, 650, 400);
		
		stage.setTitle("Dice Game");
		stage.setScene(sc);
		
		//window appearance
		VBox vb = new VBox();
		TextField bet = new TextField("0");
		Text currentAmount = new Text(startMoney+"");
		Text winOrLoss = new Text("0");
		Button small = new Button("small");
		Button large = new Button("large");
		
		BetResult result = new BetResult();
		
		//listeners
		Player p1 = new Player(bet, currentAmount, winOrLoss, result, small.getText(), Integer.parseInt(bet.getText()));
		Player p2 = new Player(bet, currentAmount, winOrLoss, result, large.getText(), Integer.parseInt(bet.getText()));
		
		small.setOnAction(p1);
		large.setOnAction(p2);
		
		root.getChildren().addAll(vb);
		vb.getChildren().addAll(bet, currentAmount, winOrLoss, small, large);
		
		stage.show();
	}
	
	public static void main(String[] args) {
		Application.launch(args);
	}
}
